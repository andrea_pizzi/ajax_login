<!-- Code for login / logout button, include it on your theme ( usally in header.php ) -->

<!-- <?php //if (is_user_logged_in()) { ?>
    <a class="login_button" href="<?php //echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php //} else { ?>
    <a class="login_button" id="show_login" href="">Login</a>
<?php // } ?> -->

<!--  Add to functions.php for including the necessary scripts -->

<?php //require_once(get_template_directory() . '/modules/ajax_login/include_files.php'); ?>

<div id="login_wrp">
<form id="login" action="login" method="post">
        <h2 class="form_title orange maintitle"><?php _e('Reserved Area Login', 'red33'); ?></h2>
        <p class="form_title orange reg_user_btn"><small> <i class="fa fa-sign-in" aria-hidden="true"></i>    <?php _e('Insert user and password, for access to private area', 'red33'); ?></small></p>
        <div class="login_form_wrp">
        <p class="status"></p>
        <input id="username" type="text" name="username" placeholder="Username">
        <input id="password" type="password" name="password" placeholder="Password">
        <input class="submit_button" type="submit" value="<?php _e('Login', 'red33'); ?>" name="submit">
        <a class="close left" href=""><i class="fa fa-times-circle-o" aria-hidden="true"></i> <?php _e('Close', 'red33'); ?></a>
        <a class="lost right" href="<?php echo wp_lostpassword_url(); ?>"><i class="fa fa-lock" aria-hidden="true"></i><?php _e('Lost Password ?', 'red33'); ?></a>
        </div>
        <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
</form>
<span class="divider"> </span>
<p class="form_title orange reg_user_btn"><small><i class="fa fa-user-plus" aria-hidden="true"></i> <?php _e('or if new users click for registered', 'red33'); ?></small></p>
<div class="vb-registration-form">
  <form class="form-horizontal registraion-form" role="form">
 
    <div class="form-group">
      <input type="text" name="vb_name" id="vb_name" value="" placeholder="Your Name" class="form-control" />
    </div>
 
    <div class="form-group">
      <input type="email" name="vb_email" id="vb_email" value="" placeholder="Your Email" class="form-control" />
    </div>
 
    <div class="form-group">
      <input type="text" name="vb_nick" id="vb_nick" value="" placeholder="Your Nickname" class="form-control" />
    </div>
 
    <div class="form-group">
      <input type="text" name="vb_username" id="vb_username" value="" placeholder="Choose Username" class="form-control" />
      <p class="help-block"><small><?php _e('Minimum 5 characters', 'red33'); ?></small></p>
    </div>
 
    <div class="form-group">
      <input type="password" name="vb_pass" id="vb_pass" value="" placeholder="Choose Password" class="form-control" />
      <p class="help-block"><small><?php _e('Minimum 8 characters', 'red33'); ?></small></p>
    </div>
 
    <?php //wp_nonce_field('vb_new_user','vb_new_user_nonce', true, true ); ?>
    <?php wp_nonce_field( 'ajax-login-nonce', 'security' , true, true ); ?>
 
    <input type="submit" class="submit_button" id="btn-new-user" value="<?php _e('Register', 'red33'); ?>" />
  </form>
</div>
 
</div>